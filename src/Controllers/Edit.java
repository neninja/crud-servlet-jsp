package Controllers;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Produto;
import Repositories.ProdutoRepositoryArray;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("id") == null) {
			request.getRequestDispatcher("/Index").forward(request, response);			
		} else {
			int id = Integer.parseInt(request.getParameter("id"));
			ProdutoRepositoryArray repo = new ProdutoRepositoryArray();
			Produto produto = repo.Details(id);
			request.setAttribute("produto", produto);
			request.getRequestDispatcher("/Edit.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("id") == null || request.getParameter("nome") == null || request.getParameter("valor") == null) {
			request.getRequestDispatcher("/default.jsp").forward(request, response);
		} else {
			
			int id = Integer.parseInt(request.getParameter("id"));
			String nome = request.getParameter("nome");
			double valor = Double.parseDouble(request.getParameter("valor"));
			
			ProdutoRepositoryArray repo = new ProdutoRepositoryArray();
			Produto produto = new Produto(nome, valor, id);
			repo.Update(produto);
			response.sendRedirect("/CRUD/Index");		
		}
	}

}
